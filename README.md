# IBK_Auth
### Setup
```angular2
python -m grpc.tools.protoc --proto_path=. --python_out=. --grpc_python_out=. id_card_authentication.proto
```

### Docker Image
```angular2
docker pull docker.maum.ai:443/brain/ibk_authentication:v0-engine

or

git clone https://pms.maum.ai/bitbucket/scm/brain/brain_idcard_web.git
docker build -f Dockerfile -t docker.maum.ai:443/brain/ibk_authentication:v0-engine
```

### Docker run command
```angular2
docker run -itd --ipc=host -p 11000:11000 --network bridge --gpus all --name ibk_engine docker.maum.ai:443/brain/ibk_authentication:v0-engine
```

### Run Server
```angular2
python server.py --model MODEL_PATH --gpuid GPUID --port PORT --log_level LOG_LEVEL`, `MODEL_PATH
```

### Run Client
```angular2
python client.py -r GRPC_IP_PORT -i IMG_PATH`, `IMG_PATH
```

### Demo Web page
```angular2
docker pull docker.maum.ai:443/brain/ibk_authentication:v0-web

or

git clone https://pms.maum.ai/bitbucket/scm/brain/brain_idcard_web.git
```

### Test img, model weight :  
https://drive.google.com/drive/folders/1wyvXQ4RfoWwaC0HCn5DMfKn7xi-bsb4C?usp=sharing
  
    
위변조 판별 모형에 들어가는 이미지는, 인지소프트 모듈을 타고 저장된 이미지로 1280 X 826크기의 jpg파일로 저장됩니다.  
그렇기에, 이미지 크기는 평균적으로 2MB를 넘지 않습니다.

실행 명령어
python idcard_auth.py --model './weight/idcard_auth.pth' --input ./test_img/monitor.jpg

### Input Image (학습이 끝나고, 제 사진으로 만든 데이터를 인지소프트 모듈에 태워 저장한 사진들입니다.)  

Paper사진  
-- ![input paper](./readme_img/paper.jpg)

Monitor 사진  
-- ![input_monitor](./readme_img/monitor.jpg)

Real 사진  
-- ![input_real](./readme_img/original.jpg)


### Output Image (코드에선 pred_result.jpg 로 저장됨)  

Paper Output사진  
-- ![output_paper](./readme_img/paper_result.jpg)

Monitor Output사진  
-- ![output_monitor](./readme_img/monitor_result.jpg)

Real Output사진  
-- ![output_real](./readme_img/real_result.jpg)



Monitor로 해당 패치를 분류시 Red.  
Paper로 해당 패치를 분류시 Blue.  
Real로 해당 패치를 분류시 그대로 표시했습니다.  

### Source 실행 Output Image  
-- ![source_result](./readme_img/result_screenshot.png)

지금은 patches중 max로 판별하고 있습니다.
