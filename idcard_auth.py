# -*- coding: utf-8 -*-
from __future__ import print_function, division
import numpy as np
import os, argparse, itertools, time, copy, logging
import torch
from torchvision import datasets, models, transforms
import cv2
from PIL import Image
import torch.nn.functional as F
import time

class IDcardAuth:
    def __init__(self, args):
        self.model = None
        self.checkpoint_path = args.model
        self.device = torch.device("cuda:%s"%args.gpuid if torch.cuda.is_available() else "cpu")
        print("using ", self.device)
        self.load_model()
        self.data_transforms = transforms.Compose([
            transforms.Resize((224, 224)),
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
        ])

    def load_model(self):
        self.model = torch.load(self.checkpoint_path)
        self.model.to(self.device)


    def determination(self, img_path):
        img = cv2.imread(img_path)
        h, w, c = img.shape

        h_range = np.linspace(0, h, 4, dtype=int)# if h == 3, [0, 1, 2, 3]
        w_range = np.linspace(0, w, 5, dtype=int)# if w == 4, [0, 1, 2, 3, 4]

        # make image 12 patches
        batches = []
        for patch_w_idx in range(len(w_range) - 1):
            for patch_h_idx in range(len(h_range) - 1):

                # define patch area
                h_min, h_max = h_range[patch_h_idx], h_range[patch_h_idx + 1]
                w_min, w_max = w_range[patch_w_idx], w_range[patch_w_idx + 1]

                # get image patch
                image = img[h_min:h_max, w_min:w_max]

                # image patch convert to model input format
                image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
                image = Image.fromarray(image)
                image = self.data_transforms(image).float()
                image = torch.unsqueeze(image, 0)
                batches.append(image)

        inputs = torch.cat(batches, dim=0)
        with torch.no_grad():
            self.model.eval()
            outputs = F.softmax(self.model(inputs.to(self.device)), dim=1)
            _, preds = torch.max(outputs, 1)
        return torch.argmax(torch.bincount(preds))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--model", type=str, default='./weight/idcard_auth.pth',
                        help="name of the model which is used for test")
    parser.add_argument("--input", type=str, required=True, help='input image path')
    parser.add_argument("--gpuid", type=str, default='0', help='gpu id')
    args = parser.parse_args()

    idcard_auth = IDcardAuth(args)

    final_pred = idcard_auth.determination(args.input)
    print(['monitor', 'paper', 'real'][int(final_pred.item())])
